python-bayespy (0.5.18-3) unstable; urgency=medium

  * QA Upload
  * Fix FTBFS with recent matplotlib (Closes: #954590)
    - Add build and test dependencies on python3-pytest
    - Adjust absolute tolerance for some tests
  * Switch to debhelper 13
  * Set Rules-Requires-Root: no
  * Bump Standards-Version to 4.5.0, no changes

 -- Graham Inggs <ginggs@debian.org>  Thu, 09 Jul 2020 14:34:54 +0000

python-bayespy (0.5.18-2) unstable; urgency=medium

  * QA upload.
  * [d7c620d] Update standards version to 4.4.1, no changes needed.
  * [d35bb12] Bump debhelper from old 11 to 12.
  * [69bceed] Relax build-depends versions
  * [a689689] Orphan the package.
  * [ccecbe3] Drop dependency on dpkg-dev

 -- Anton Gladky <gladk@debian.org>  Fri, 01 Nov 2019 20:15:29 +0100

python-bayespy (0.5.18-1) unstable; urgency=medium

  * Team upload
  * New upstream version 0.5.18

 -- Ghislain Antony Vaillant <ghisvail@gmail.com>  Sun, 31 Mar 2019 14:03:12 +0200

python-bayespy (0.5.17-2) unstable; urgency=medium

  * Team upload
  * Drop myself from uploaders
  * Add missing test dependency on matplotlib.
    Thanks to Steve Langasek (Closes: #884516, #907221)
  * Fix obsolete declaration of autodep8 tests
  * Bump standards version to 4.3.0

 -- Ghislain Antony Vaillant <ghisvail@gmail.com>  Sat, 30 Mar 2019 19:09:30 +0100

python-bayespy (0.5.17-1) unstable; urgency=medium

  * Team upload.
  * Point watch file to secure URI
  * Rename debian/tests/control to control.autodep8
  * Add missing Depends: python3-tk
    Closes: #896210
  * debhelper 11
  * Point Vcs fields to salsa.debian.org
  * Standards-Version: 4.1.4
  * Testsuite: autopkgtest-pkg-python
  * Remove ancient field X-Python3-Version
  * Remove unneeded get-orig-source-target

 -- Andreas Tille <tille@debian.org>  Mon, 21 May 2018 12:55:12 +0200

python-bayespy (0.5.12-1) unstable; urgency=medium

  * New upstream version 0.5.12
  * Bump the standards version to 4.1.1
  * Add recommended get-orig-source target

 -- Ghislain Antony Vaillant <ghisvail@gmail.com>  Tue, 31 Oct 2017 09:10:19 +0000

python-bayespy (0.5.11-1) unstable; urgency=medium

  * New upstream version 0.5.11
  * Update copyright dates
  * Revert exclusion of plot tests
  * Use DEB_BUILD_OPTIONS for nocheck
  * Replace occurrences of findstring with filter
  * Fixup whitespacing in rules file
  * Move extend-diff-ignore to d/s/options
  * Add an autopkgtest suite
  * Fixup the Vcs-Browser URI
  * Drop install dependency on python3-tk
  * Bump the standards version to 4.1.0

 -- Ghislain Antony Vaillant <ghisvail@gmail.com>  Wed, 27 Sep 2017 16:41:09 +0100

python-bayespy (0.5.8-1) unstable; urgency=low

  * Initial release. (Closes: #862198)

 -- Ghislain Antony Vaillant <ghisvail@gmail.com>  Sat, 13 May 2017 11:11:08 +0100
